import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Bancomat machina = new Bancomat(51000);

        machina.give();

        System.out.println("Остаток денежных средств: " + machina.getBalance() + "\n");

        double valueToPut;
        System.out.print("Введите сумму которое вы хотите положить: ");
        valueToPut = scanner.nextDouble();

        machina.put(valueToPut);

        System.out.println("\n\nКол-во проведённых операций: " + machina.getQuantity() + "\nВ банкомате осталось: " + machina.getBalance());
    }
}

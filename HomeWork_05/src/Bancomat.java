import java.util.Scanner;

public class Bancomat {

    public final static double MAX_AMOUNT_TO_WITHDRAWAL = 50000;
    public final static double MAX_AMOUNT_OF_MONEY = 5000000;
    public final static double DEFAULT_AMOUNT_OF_MONEY = 0;

    private double balance;
    private int quantity = 0;

    Bancomat(double balance) {
        if (balance <= MAX_AMOUNT_OF_MONEY) {
            this.balance = balance;
        } else {
            this.balance = DEFAULT_AMOUNT_OF_MONEY;
        }
    }

    void give() {
        boolean i = true;
        double value;
        Scanner scanner = new Scanner(System.in);
        while (i) {
            System.out.print("Введите сумму, которую вы хотите снять: ");
            value = scanner.nextDouble();
            if (value <= MAX_AMOUNT_TO_WITHDRAWAL && value <= balance) {
                System.out.println("Выдано " + value + " руб.");
                balance -= value;
                quantity++;
                i = false;
            } else if (value > MAX_AMOUNT_TO_WITHDRAWAL) {
                System.out.println("Вы превысили максимально разрешенную сумму к выдаче! Введите сумму не более 50 000 руб.!");
            } else {
                System.out.println("Недостаточно средств для выдачи! Доступный остаток: " + balance + " руб.\nПовторите операцию!");
            }
        }
    }

    void put(double value) {
        if (balance + value <= MAX_AMOUNT_OF_MONEY) {
            System.out.println("На счёт положено " + value + "руб. \nОперация завершена!");
            balance += value;
        } else {
            double excessPut = value - (MAX_AMOUNT_OF_MONEY - balance);
            System.out.println("Достигнут максимальный лимит! На счёт положено " + (value - excessPut) + "руб.\nВозвращено " + excessPut + "руб.\nОперация завершена!");
            balance += (value - excessPut);
        }
        quantity++;
    }

    int getQuantity() {
        return this.quantity;
    }

    double getBalance() {
        return this.balance;
    }
}
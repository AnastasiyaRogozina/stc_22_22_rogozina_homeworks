INSERT INTO driver (first_name, last_name, phone_number, experience, age, is_drivers_license, categorydl, rating)
VALUES ('Ivan', 'Petrov', '8-999-555-22-33', 16, 35, TRUE, 'B, C, D', 5),
       ('Tanya', 'Ivanova', '8-999-999-99-99', 3, 25, TRUE, 'B', 3.5),
       ('Mikhail', 'Nicolaev', '8-222-222-22-22', 10, 40, TRUE, 'B, C, D', 4.9),
       ('Egor', 'Sidorov', '8-921-666-44-33', 0, 20, TRUE, 'B', 0),
       ('Anna', 'Vasileva', '8-911-444-44-66', 2, 25, TRUE, 'B', 4);

INSERT INTO car (model, color, number_car, car_owner_id)
VALUES ('Audi Q7', 'red', 'B888BB178', 3),
       ('Land Rover Sport', 'black', 'A999AA198', 1),
       ('Skoda Octavia', 'grey', 'C555CC78', 2),
       ('BMW X-6', 'blue', 'E777EE147', 4),
       ('Geep', 'green', 'K444KK', 5);

INSERT INTO trip (driver_id, car_id, data_trip, length_trip)
VALUES (2, 1, '2022-11-10', '00:20'),
       (3, 2, '2022-11-15', '01:30'),
       (1, 5, '2022-11-14', '00:45'),
       (4, 4, '2022-11-16', '01:20'),
       (5, 2, '2022-11-9', '01:00');


-- Ошибочные запросы

-- нет водительского удостоверения
--INSERT INTO driver (first_name, last_name, phone_number, age, is_drivers_license)
--VALUES ('Masha', 'Baranova', '8-999-111-11-11', 19, FALSE);

-- возраст меньше 18 лет
--INSERT INTO driver (first_name, last_name, age, phone_number, is_drivers_license)
--VALUES ('Masha', 'Baranova', 16, TRUE);
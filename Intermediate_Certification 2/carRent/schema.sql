DROP TABLE IF EXISTS trip;
DROP TABLE IF EXISTS car;
DROP TABLE IF EXISTS driver;


CREATE TABLE driver
(
    id                 BIGSERIAL PRIMARY KEY,
    first_name         VARCHAR(20) NOT NULL,
    last_name          VARCHAR(20) NOT NULL,
    phone_number       VARCHAR(20) UNIQUE NOT NULL,  --длину делаю с запасом, предполагаю, что может использоваться иностранный номер
    experience         INTEGER CHECK ( experience >= 0 AND experience < (age - 18) ) DEFAULT 0,
    age                INTEGER CHECK (age >= 18 AND age <= 90) NOT NULL,
    is_drivers_license BOOL DEFAULT FALSE,
    categoryDL         VARCHAR(20) CHECK ( is_drivers_license IS TRUE),
    rating             DOUBLE PRECISION CHECK (rating >= 0 AND rating <= 5) DEFAULT 0
);

CREATE TABLE car
(
    id           BIGSERIAL PRIMARY KEY,
    model        VARCHAR(20) NOT NULL,
    color        VARCHAR(20) NOT NULL,
    number_car   VARCHAR(15) UNIQUE NOT NULL, -- беру с запасом, полагая, что номера разной длины бывают
    car_owner_id BIGINT NOT NULL,
    FOREIGN KEY (car_owner_id) REFERENCES driver (id)
);

CREATE TABLE trip
(
    id          BIGSERIAL PRIMARY KEY,
    driver_id   BIGINT,
    FOREIGN KEY (driver_id) REFERENCES driver (id),
    car_id      BIGINT,
    FOREIGN KEY (car_id) REFERENCES car (id),
    data_trip   DATE,
    length_trip TIME
);

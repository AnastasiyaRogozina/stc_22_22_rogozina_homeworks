package moduleJavaCore;

import java.util.List;

public interface ProductsRepository {
    Product findById(int id);

    List<Product> findAllByTitleLike(String title);

    void update(Product product);
}

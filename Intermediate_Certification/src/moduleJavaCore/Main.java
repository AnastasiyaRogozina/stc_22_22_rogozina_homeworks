package moduleJavaCore;

public class Main {
    public static void main(String[] args) {
        ProductsRepository productsRepository = new ProductsRepositoryFileBaseImpl("Products");

        System.out.println("\n1. Вернём продукт под id = 8:\n" + productsRepository.findById(8));

        System.out.println("\n   Вернём продукт под id = 25, зная, что в списке отсутствует.\n   Возвращает: " + productsRepository.findById(25));

        System.out.println("\n2. Вернём список всех продуктов, содержащих в названии 'il':\n" + productsRepository.findAllByTitleLike("il"));

        System.out.println("\n3. Начальная стоимость и количество товара под id = 1:\n" + productsRepository.findById(1)); // 1|Milk|39.8|5 -> 1|Milk|40.1|10

        Product milk = productsRepository.findById(1);

        milk.setPrice(40.1);
        milk.setCount(10);

        productsRepository.update(milk);

        System.out.println("\n   Изменённая стоимость и количество товара под id = 1:\n" + productsRepository.findById(1));

    }
}
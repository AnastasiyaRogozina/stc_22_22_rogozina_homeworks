package moduleJavaCore;

import java.io.*;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductsRepositoryFileBaseImpl implements ProductsRepository {

    private final String fileName;

    public ProductsRepositoryFileBaseImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapper = current -> {
        String[] parts = current.split("\\|");
        int id = Integer.parseInt(parts[0]);
        String title = parts[1];
        double price = Double.parseDouble(parts[2]);
        int count = Integer.parseInt(parts[3]);

        return new Product(id, title, price, count);
    };

    private static final Function<Product, String> productToStringMapper = product ->
            product.getId() + "|" +
                    product.getTitle() + "|" +
                    product.getPrice() + "|" +
                    product.getCount();


    @Override
    public Product findById(int id) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            return bufferedReader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> Objects.equals(product.getId(), id))
                    .findFirst().orElse(null);
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileData(e);
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            return bufferedReader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getTitle().contains(title))
                    .toList();

        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileData(e);
        }
    }

    @Override
    public void update(Product product) {
        try {
            List<Product> currentProductList = new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .collect(Collectors.toList());

            for (Product productChanged : currentProductList) {
                if (productChanged.getId() == product.getId()) {
                    currentProductList.set(currentProductList.indexOf(productChanged), product);
                }
            }

            FileWriter fileWriter = new FileWriter(fileName, false);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for (Product productNew : currentProductList) {
                String newProduct = productToStringMapper.apply(productNew);
                bufferedWriter.write(newProduct);
                bufferedWriter.newLine();
            }

            bufferedWriter.close();
            fileWriter.close();

        } catch (IOException e) {
            throw new UnsuccessfulWorkWithFileData(e);
        }
    }
}



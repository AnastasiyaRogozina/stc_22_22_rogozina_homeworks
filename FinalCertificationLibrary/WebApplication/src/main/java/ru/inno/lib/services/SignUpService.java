package ru.inno.lib.services;

import ru.inno.lib.dto.UserForm;

public interface SignUpService {
    void signUp(UserForm userForm);
}

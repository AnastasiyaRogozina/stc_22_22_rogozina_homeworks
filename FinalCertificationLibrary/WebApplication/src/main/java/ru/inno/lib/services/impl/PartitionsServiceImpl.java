package ru.inno.lib.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.lib.models.Partition;
import ru.inno.lib.models.User;
import ru.inno.lib.repositories.PartitionsRepository;
import ru.inno.lib.repositories.UsersRepository;
import ru.inno.lib.services.PartitionsService;

import java.util.List;
import java.time.LocalDate;
import java.util.List;


@RequiredArgsConstructor
@Service
public class PartitionsServiceImpl implements PartitionsService {

    private final PartitionsRepository partitionsRepository;
    private final UsersRepository usersRepository;

    @Override
    public void addReaderToPartition(Long partitionId, Long readerId) {
        Partition partition = partitionsRepository.findById(partitionId).orElseThrow();
        User reader = usersRepository.findById(readerId).orElseThrow();

        reader.getPartitions().add(partition);

        usersRepository.save(reader);
    }

    @Override
    public Partition getPartition(Long partitionId) {
        return partitionsRepository.findById(partitionId).orElseThrow();
    }

    @Override
    public List<User> getNotInPartitionReaders(Long partitionId) {
        Partition partition = partitionsRepository.findById(partitionId).orElseThrow();
        return usersRepository.findAllByPartitionsNotContainsAndState(partition, User.State.CONFIRMED);
    }

    @Override
    public List<User> getInPartitionReaders(Long partitionId) {
        Partition partition = partitionsRepository.findById(partitionId).orElseThrow();
        return usersRepository.findAllByPartitionsContains(partition);
    }


}

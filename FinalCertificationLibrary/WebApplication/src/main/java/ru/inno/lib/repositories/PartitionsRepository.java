package ru.inno.lib.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.lib.models.Partition;

import java.util.List;

public interface PartitionsRepository extends JpaRepository<Partition, Long> {
    //List<Partition> findAllByStateNot(Partition.State state);
}

package ru.inno.lib.models;

import javax.persistence.*;
import lombok.*;

import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(exclude = "partition")
@ToString(exclude = "partition")
public class Book {
    public enum State {
        READY, NOT_READY, DELETED;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    @Column(length = 1000)
    private String summary;

    @Column(name = "start_time")
    private LocalTime startTime;

    @Column(name = "finish_time")
    private LocalTime finishTime;

    @ManyToOne
    @JoinColumn(name = "partition_id")
    private Partition partition;

    @Enumerated(value = EnumType.STRING)
    private State state;

}

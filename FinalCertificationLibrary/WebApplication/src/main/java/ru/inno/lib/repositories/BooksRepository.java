package ru.inno.lib.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.lib.models.Book;


import java.util.List;

public interface BooksRepository extends JpaRepository<Book, Long> {
    List<Book> findAllByStateNot(Book.State state);

    //List<Book> findAllByPartitionNullAndStateNot(Book.State state);

   // List<Book> findAllByPartitionAndStateNot(Partition partition, Book.State state);

}

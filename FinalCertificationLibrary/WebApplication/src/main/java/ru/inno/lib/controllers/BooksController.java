package ru.inno.lib.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.lib.dto.BookForm;
import ru.inno.lib.services.BooksService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/books")
public class BooksController {

        private final BooksService booksService;

        @GetMapping
        public String getBooksPage(Model model) {
                model.addAttribute("books", booksService.getAllBooks());
                return "books";
        }


        @GetMapping("/{book-id}/delete")
        public String deleteBook(@PathVariable("book-id") Long bookId) {
                booksService.deleteBook(bookId);
                return "redirect:/books";
        }

        @PostMapping
        public String addBook(BookForm book) {
                booksService.addBook(book);
                return "redirect:/books";
        }

        @PostMapping("/{book-id}/update")
        public String updateBook(@PathVariable("book-id") Long bookId,
                                 BookForm book) {
                booksService.updateBook(bookId, book);
                return "redirect:/{book-id}";
        }

        @PostMapping("/{book-id}/readers")
        public String addReaderToBook(@PathVariable("book-id") Long bookId,
                                           @RequestParam("reader-id") Long readerId) {
                booksService.addReaderToBook(bookId, readerId);
                return "redirect:/books/" + bookId;
        }

        @GetMapping("/{book-id}")
        public String getBookPage(@PathVariable("book-id") Long bookId, Model model) {
                model.addAttribute("book", booksService.getBook(bookId));
                model.addAttribute("notInBookReaders", booksService.getNotInBookReaders(bookId));
                model.addAttribute("inBookReaders", booksService.getInBookReaders(bookId));
                return "book";
        }

}

package ru.inno.lib.services;

import ru.inno.lib.dto.UserForm;
import ru.inno.lib.models.User;

import java.util.List;

public interface UsersService {
    List<User> getAllUsers();

    void addUser(UserForm user);

    User getUser(Long id);

    void updateUser(Long userId, UserForm user);

    void deleteUser(Long userId);
}

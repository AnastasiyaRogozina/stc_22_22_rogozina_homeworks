package ru.inno.lib.models;

import javax.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"books", "readers"})
@Entity
public class Partition {

    public enum State {
        READY, NOT_READY, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Column(length = 1000)
    private String description;

    private LocalDate start;
    private LocalDate finish;

    @OneToMany(mappedBy = "partition", fetch = FetchType.EAGER)
    private Set<Book> books;

    @ManyToMany(mappedBy = "partitions", fetch = FetchType.EAGER)
    private Set<User> users;

    @Enumerated(value = EnumType.STRING)
    private State state;

}

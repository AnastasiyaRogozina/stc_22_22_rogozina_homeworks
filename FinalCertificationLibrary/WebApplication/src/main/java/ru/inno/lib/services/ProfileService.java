package ru.inno.lib.services;

import ru.inno.lib.models.User;
import ru.inno.lib.security.details.CustomUserDetails;

public interface ProfileService {
    User getCurrent(CustomUserDetails userDetails);
}

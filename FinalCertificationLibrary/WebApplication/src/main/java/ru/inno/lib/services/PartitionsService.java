package ru.inno.lib.services;

import ru.inno.lib.models.Partition;
import ru.inno.lib.models.User;

import java.util.List;

public interface PartitionsService {
    void addReaderToPartition(Long partitionId, Long readerId);

    Partition getPartition(Long partitionId);

    List<User> getNotInPartitionReaders(Long partitionId);

    List<User> getInPartitionReaders(Long partitionId);
}

package ru.inno.lib.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.lib.models.Partition;
import ru.inno.lib.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByStateNot(User.State state);

    List<User> findAllByPartitionsNotContainsAndState(Partition partition, User.State state);

    List<User> findAllByPartitionsContains(Partition partition);

    Optional<User> findByEmail(String email);
}

package ru.inno.lib.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.lib.models.Partition;
import ru.inno.lib.security.details.CustomUserDetails;
import ru.inno.lib.services.PartitionsService;

import java.util.List;

@RequiredArgsConstructor
@Controller
@RequestMapping("/partitions")
public class PartitionsController {

    private final PartitionsService partitionsService;

    @GetMapping
    public String getPartitionsPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("partitions",
                List.of(
                        Partition.builder().id(1L).title("Детская литература").build(),
                        Partition.builder().id(2L).title("Поэзия").build(),
                        Partition.builder().id(3L).title("Художественная литература").build()
                ));
        return "partitions_page";
    }

       @PostMapping("/{partition-id}/readers")
    public String addReaderToPartition(@PathVariable("partition-id") Long partitionId,
                                     @RequestParam("reader-id") Long readerId) {
           partitionsService.addReaderToPartition(partitionId, readerId);
        return "redirect:/partitions/" + partitionId;
    }

    @GetMapping("/{partition-id}")
    public String getPartitionPage(@PathVariable("partition-id") Long partitionId, Model model) {
        model.addAttribute("partition", partitionsService.getPartition(partitionId));
        model.addAttribute("notInPartitionReaders", partitionsService.getNotInPartitionReaders(partitionId));
        model.addAttribute("inPartitionReaders", partitionsService.getInPartitionReaders(partitionId));
        return "partition_page";
    }

}

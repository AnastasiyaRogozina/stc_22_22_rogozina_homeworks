package ru.inno.lib.services;

import ru.inno.lib.dto.BookForm;

import ru.inno.lib.models.Book;
import ru.inno.lib.models.User;


import java.util.List;

public interface BooksService {

    List<Book> getAllBooks();

    void addBook(BookForm book);

    List getBook(Long id);

    void updateBook(Long bookId, BookForm book);

    void deleteBook(Long bookId);

    void addReaderToBook(Long bookId, Long readerId);

    Object getNotInBookReaders(Long bookId);

    Object getInBookReaders(Long bookId);

    List<User> getInPartitionReaders(Long partitionId);

}



package ru.inno.lib.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.lib.dto.BookForm;
import ru.inno.lib.models.Book;
import ru.inno.lib.models.User;
import ru.inno.lib.repositories.BooksRepository;
import ru.inno.lib.services.BooksService;

import java.util.List;

@RequiredArgsConstructor
@Service

public class BooksServiceImpl implements BooksService {

    private final BooksRepository booksRepository;



    @Override
    public List<Book> getAllBooks()  {
        return booksRepository.findAllByStateNot(Book.State.DELETED);
    }

    @Override
    public void addBook(BookForm book) {
        Book newBook = Book.builder()
                .state(Book.State.NOT_READY)
                .name(book.getName())
                .summary("книги")
                .build();
        booksRepository.save(newBook);

    }

    @Override
    public List getBook(Long id) {
        return null;
    }

    @Override
    public void updateBook(Long bookId, BookForm book) {

    }

    @Override
    public void deleteBook(Long bookId) {

    }

    @Override
    public void addReaderToBook(Long bookId, Long readerId) {
    }

    @Override
    public Object getNotInBookReaders(Long bookId) {
        return null;
    }

    @Override
    public Object getInBookReaders(Long bookId) {
        return null;
    }

    @Override
    public List<User> getInPartitionReaders(Long partitionId) {
        return null;
    }
}

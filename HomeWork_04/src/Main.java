import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Задание один: ");
        System.out.println("Введите левую границу чисел");
        int left = scanner.nextInt();

        System.out.println("Введите правую границу чисел");
        int right = scanner.nextInt();
        System.out.println("Сумма чисел:");
        int sum = sumRange(left, right);
        System.out.println(sum);

        System.out.println("Задание два: ");
        System.out.println("Четные элементы массива: ");
        int[] evenArray = {15, 6, 8, 3, 9, 8, 20, 14, 7};
        evenArray(evenArray);
    }

    public static int sumRange(int left, int right) {
        if (left < 0 || right < left) {
            return -1;
        }
        int sum = 0;
        for (int i = left; i <= right; i++) {
            sum += i;
        }
        return sum;
    }
    public static void evenArray(int[] a) {

        for (int i = 0; i < a.length; i++) {
            if (a[i] % 2 == 0) {
                System.out.print(a[i] + " ");
            }
        }
    }

}
